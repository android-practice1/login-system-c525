package com.squaredevops.loginsystemc525;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "userdetails.db";
    private static final String TABLE_NAME = "user_details";
    private static final String ID = "id"; // column 0
    private static final String NAME = "Name"; // column 1
    private static final String EMAIL = "Email"; // column 2
    private static final String USERNAME = "Username"; // column 3
    private static final String PASSWORD = "Password"; // column 4
    private static final int VERSION_NUMBER = 1;
    private Context context;
    UserDetails userDetails;

    private static final String CREATE_TABLE = "CREATE TABLE " +
            TABLE_NAME + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NAME + " VCARCHAR(255) NOT NULL, " +
            EMAIL + " TEXT NOT NULL, " +
            USERNAME + " TEXT NOT NULL, " +
            PASSWORD  + " TEXT NOT NULL)";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_NUMBER);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(CREATE_TABLE);
            Toast.makeText(context, "onCreate is called", Toast.LENGTH_SHORT).show();
        } catch (Exception exception) {
            Toast.makeText(context, "Exception : " + exception, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try {
            Toast.makeText(context, "onUpgrade is called", Toast.LENGTH_SHORT).show();
            sqLiteDatabase.execSQL(DROP_TABLE);
            onCreate(sqLiteDatabase);

        } catch (Exception exception) {
            Toast.makeText(context, "Exception : " + exception, Toast.LENGTH_SHORT).show();
        }
    }

    public long insertData(UserDetails userDetails) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(NAME, userDetails.getName());
        contentValues.put(USERNAME, userDetails.getUsername());
        contentValues.put(EMAIL, userDetails.getEmail());
        contentValues.put(PASSWORD, userDetails.getPassword());

        long rowid = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);

        return rowid;
    }

    // matching login data
    public Boolean findPassword(String UserName, String PassWord) {

        // getting data from database
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        Boolean result = false;

        if (cursor.getCount() == 0) {
            Toast.makeText(context, "No data is found", Toast.LENGTH_SHORT).show();
        } else {

            while (cursor.moveToNext()) {
                String userName = cursor.getString(3); // getting data from column 3 (username)
                String passWord = cursor.getString(4); // getting data from column 4 (password)

                if (userName.equals(UserName) && passWord.equals(PassWord)) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

}
