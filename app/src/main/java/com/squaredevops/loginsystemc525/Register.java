package com.squaredevops.loginsystemc525;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {

    EditText name, username, email, password;
    Button submit;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        submit = (Button) findViewById(R.id.submit_button);
        name = (EditText) findViewById(R.id.register_name);
        username = (EditText) findViewById(R.id.register_username);
        email = (EditText) findViewById(R.id.register_email);
        password = (EditText) findViewById(R.id.register_password);

        UserDetails userDetails = new UserDetails();
        databaseHelper = new DatabaseHelper(this);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String only_name = name.getText().toString();
                String u_name = username.getText().toString();
                String mail = email.getText().toString();
                String pass = password.getText().toString();

                userDetails.setName(only_name);
                userDetails.setUsername(u_name);
                userDetails.setEmail(mail);
                userDetails.setPassword(pass);

                long rowid = databaseHelper.insertData(userDetails);

                if (rowid > 0) {
                    Toast.makeText(getApplicationContext(), "Row : " + rowid, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "failed to insert data", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}